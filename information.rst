General Information
************************


Resources
==========
- General info: http://isa-tools.org
- Tools' overview in this short paper: http://bioinformatics.oxfordjournals.org/content/26/18/2354.full.pdf+html
- Issue tracking and bug reporting: https://github.com/ISA-tools/ISAcreator/issues
- Mainline source code: https://github.com/ISA-tools/ISAcreator
- Twitter: http://twitter.com/isatools
- IRC: irc://irc.freenode.net/#isatab
- Blog: http://isatools.wordpress.com


Why use ISAcreator?
==========

#. You are a lab scientist recording experimental information and need to meet annotation requirements?

#. You are planning an experiment and would to organize you experimental records from the start?

#. You need to deposit experimental data to public repositories to obtain accession number for your manuscript?
