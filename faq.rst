Frequently Asked Questions
**************************

**Q: ISAcreator is struggling on larger files, how can I stop this.**

A: This is generally because of memory and Java heap space. You can increase the amount of memory available to ISAcreator through running it from the command line like so
	
	::

		java -jar ISAcreator.jar -Xms256M -Xmx1024m

This will allocate 256m of RAM to ISAcreator when it starts, and allow it to consume up to 1GB, plenty to deal with your large files.


**Q: I had some problems with the GUI on linux (Ubuntu 12.04). (for example, a drop-down menu with options hidden behind the window in foreground).**

A: It seems to be a problem with the JDK used: I had the openjdk (default installation) and I recently installed the oracle-java-jdk. With this JDK, GUI bugs seem to not occur anymore.

To install oracle-java-jdk (debian):
	
	::
		
		sudo add-apt-repository ppa:webupd8team/java
		sudo apt-get update
		sudo apt-get install oracle-java7-installer
