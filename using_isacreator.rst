Using ISAcreator
===============================================


.. toctree::
   :maxdepth: 2

ISAcreator is a piece of software developed around the ISA-TAB format, a spreadsheet based format meant to provide the necessary elements for describing the experiments, in which protocols and instruments are used to generate images, matrices of measurement points used to monitor a biological system nder study, possibly following a stimulus or some kind of stress. So the main thing is that you are no longer limited to one type of technology. If the same tissue fragment is used to gather biological signal with different techniques, you can use ISAcreator to record all those steps.

Spreadsheets are widely used in biology to track information, we have just leveraged the layout and added specific features to make annotation more effective. For instance,

#. a built in text editor, useful when describing protocols or overall study description

#. an ontology widget, which allows to scan resources from NCBO Bioportal

#. a term tagging service. Yes, we realized that the ontology widget was good for selection, but sometimes it is just nice to be able to type free text. That's ok but so much when maintaining archives and databases.

#. a bibliographic service, which fetches information from Pubmed by simply providing a pubmed identifier

#. a spreadsheet mapping tool to import legacy data already organized in Excel files for instance

#. a wizard to create templates from information about the study factors and their levels

#. a plugin architecture which allows to extend ISAcreator capabilities. We showcase this function with a plugin to generate QR codes from sample information

Creating a user account
-----------------------


.. |createprofile| image:: _static/createprofile.png
To create an account, click on |createprofile| button from the login screen to create a new user profile. 

.. image:: _static/createprofile-screen.png
    :width: 500px
    :align: center


.. note:: The information supplied in the field "institution" is used by ISAcreator Wizard component in conjunction with the ISA-default configuration that ships with the tools. Please refer to section ISAcreator Wizard and ISAconfigurator Document for in depth explanation on how.


Why do i need to register and create an account?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Registering with ISAcreator is very quick and simple. The main reason for bothering users with registration details is to be able to allow ISAcreator to learn from new users. When using ISAcreator and annotating experiments, ISAcreator remembers the history of selection for a particular user profile. So the next time that user comes around, there will be no need to type in the same information again. It is there, available. Note that ISAcreator currently does not encrypt information, even though technically, it is fairly simple to do so.

Settings
--------

Setting the http proxy
^^^^^^^^^^

To do so, click the **Settings** button from the Main Menu page. 

.. image:: _static/menu.png
    :width: 500px
    :align: center

Then select the first item **http proxy** which will display the following screen:

.. image:: _static/httpproxy.png
    :width: 500px
    :align: center

Setting default file locations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To do so, go to the **Settings** page as shown previously and click on **program file locations**.

.. image:: _static/locations.png
    :width: 500px
    :align: center


Creating a Study manually
-------------------------

To do so, from the Main Menu page, select the **create new experiment description** button. This will show the following screen offering 3 options.

.. image:: _static/create_study_menu.png
    :width: 500px
    :align: center

From this new page, select the **create manually** option. This will result in the following screen being shown.

.. image:: _static/new_study.png
    :width: 500px
    :align: center

Click on the add study button (the big **+**) to show the dialog below.

.. image:: _static/new_study_dialog.png
    :width: 500px
    :align: center

Enter a study identifier of your choosing and you will be presented with the following screen. This is your blank canvas on which to create your metadata dream.

.. image:: _static/blank_experiment.png
    :width: 500px
    :align: center

As you can see, the center page contains summary information to help you starting your submission. Before going further, let's pay attention to the layout of page and identify 4 elements:

#. A menu bar sitting on top of the window, just like any other desktop application.

#. A tree-view pane: this is where you will be able to see the list of spreadsheet related to samples and assay information.

#. An information box: in the lower left hand part of the windows, the area will be used to display annotation metadata, such as term identifier and term definitions.

#. A central pane: it makes most of the window real estate and as we will show, is where tables are presented for the users to enter information.


The spreadsheet view and its functionalities
---------------------------------------------

The spreadsheet has numerous functionalities to help you annotate your experiment.

.. image:: _static/spreadsheet.png
    :width: 500px
    :align: center

These functionalities include:

#. Row/column addition and removal
#. Sorting, both on one or two columns, ascending or descending.
#. Copy/paste - copy whole rows/columns downwards. 
#. Undo/redo
#. Autofill
#. Transposition - view and edit the spreadsheet with the columns as rows and rows as columns. This is beneficial if you have only 5/6 rows to edit.
#. Group highlighting - shows via colour the grouping on same characteristics, factors, comments, sample names, etc.
#. Highlight required fields
#. Custom cell editors - get presented with an ontology selection when ontologies are recommended, file lookup widget when files are required, a calendar when dates are required, and so on. 


Creating an ISArchive
---------------------



Visualization Components
------------------------


Highlight groups
^^^^^^^^^^^^^^^^

By right clicking on the spreadsheet, you'll be presented with a popup. Hover over highlight groups, and select the fields you wish to group. This will present a view like that shown below.

.. image:: _static/highlight-groups.png
    :width: 500px
    :align: center


Highlight treatment groups
^^^^^^^^^^^^^^^^^^^^^^^^^^

An additional visualization, which shows you a graph of the experiment along with information about each assay is used to give an idea about the 'balance' in your experiment. Ideally, each treatment group, defined by the cartesian product of factors has an equal number of samples. This visualization shows you this immediately.

.. image:: _static/treatment-groups.png
    :width: 500px
    :align: center


Advanced Functionality
----------------------

Experiment Design Wizard
^^^^^^^^^^^^^^^^^^^^^^^^

Legacy data import
^^^^^^^^^^^^^^^^^^

Merge Studies
^^^^^^^^^^^^^


Plugins
-------

ISAcreator is built upon the OSGi plugin framework. We have a plugin architecture that supports a number of operations within the tool including:

#. **Custom cell editors** - such components can be used to help users edit content in particular fields within ISAcreator. For example, `Metabolights <http://www.ebi.ac.uk/metabolights/`_ created their own plugin to help users create metabolite assignment files. 

#. **Additional vocabulary search** - this iswithin the ontology lookup interface. This is used by ToxBank and Novartis who have their own in-house vocabularies they want users to annotate with. The plugin allows them to define their own search facility, so when a user searches for 'cancer', it not only searches for cancer in BioPortal, but in Novartis' metadata facility or ToxBank's facility.

#. **Additional general functionalities** - if you want to add general UI components to the tool for visualization etc. this is also possible. For instance the Automacron plugin, used to display the experiment processing graph is built upon this plugin type. When detected in the plugin directory, it will create a menu item that will allow you to call this component.



