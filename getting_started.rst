Getting Started
==========


Requirements
------------

Prior to install ISAcreator tool, irrespective of the operating system used, do make sure that Java version 1.6 is deployed on the raget machine and that JAVA_HOME environmental variable has been set.

To check which version of Java is currently available, open a terminal and type the following command:

::

	java -version

The result should look something like this (if your Java version is above 1.6, all is well):

::

	java version "1.6.0_22" Java(TM) SE Runtime Environment (build 1.6.0_22-b04-307-10M3261) Java HotSpot(TM) 64-Bit Server VM (build 17.1-b03-307, mixed mode)


Getting the software
------------

The latest ISAcreator builds are available from `here <http://isatab.sf.net/tools.html>`_. It is available for Windows, Mac and Linux. Download the appropriate version from the aforementioned link.


Installing
------------

There is no install process per se, as soon as the zip archive is downloaded, you have ISAcreator. The first thing to do is unzip the archive. 

.. warning:: For Windows 7™ users, do ensure that the archive is properly unpacked. Windows 7 explorer seems to allow viewing the content of an archive, even starting a jar application upon double clicking when the application is still compressed. Doing so will result in an failing to use ISAcreator properly. Freeware such as 7-zip™ provides an easy means to ensure zip archive have been properly expanded.

When unarchiving is complete, just double click on the ISAcreator Jar file to launch the application. 


Running from command line
------------

More advanced users may want to use the command line to pass additional configuration parameters to the tool at start up. In particular, users manipulating large datasets may want/need to allocate more memory. To do so, proceed as follows:

#. open a terminal
#. navigate to ISAcreator installation folder
	
	::

		cd /Users/JohnSmith/ISAcreator-1.5/
	

#. enter the following (*watch out for placing spaces correctly and remember that arguments are case sensitive. Xms and Xmx parameters allow to set the minimal and maximal amount of memory available to the java virtual machine (jvm) used by ISAcreator*).
	
	::

		java -jar -Xms256m -Xmx1024m ISAcreator-<VERSION>.jar

If you see a nice splash screen showing a loading indicator, then a configuration loaf screen like the one below, you have successfully installed ISAcreator on your machine. For a more thorough description of the tool and its functionalities, please refer to the following chapters.


.. image:: _static/user_screen.png
    :width: 500px
    :align: center