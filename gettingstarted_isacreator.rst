.. ISAcreator Documentation documentation master file, created by
   sphinx-quickstart on Wed Jan 15 12:48:03 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 4

   
ISAcreator Getting Started
************************




Creating a user account
=======================


.. |createprofile| image:: _static/createprofile.png
To create an account, click on |createprofile| button from the login screen to create a new user profile. 

.. image:: _static/createprofile-screen.png
    :width: 500px
    :align: center


.. note:: The information supplied in the field "institution" is used by ISAcreator Wizard component in conjunction with the ISA-default configuration that ships with the tools. Please refer to section ISAcreator Wizard and ISAconfigurator Document for in depth explanation on how.


Why do i need to register and create an account?
------------------------------------------------

Registering with ISAcreator is very quick and simple. The main reason for bothering users with registration details is to be able to allow ISAcreator to learn from new users. When using ISAcreator and annotating experiments, ISAcreator remembers the history of selection for a particular user profile. So the next time that user comes around, there will be no need to type in the same information again. It is there, available. Note that ISAcreator currently does not encrypt information, even though technically, it is fairly simple to do so.


Settings
========

To access ISAcreator's general settings screen, click the **Settings** button from the Main Menu page. 

.. image:: _static/menu.png
    :width: 500px
    :align: center


Setting the http proxy
----------------------

From the settings screen, select the first item **http proxy** which will display the following screen:

.. image:: _static/httpproxy.png
    :width: 500px
    :align: center

Setting default file locations
------------------------------

From the settings screen and click on **program file locations**.

.. image:: _static/locations.png
    :width: 500px
    :align: center

