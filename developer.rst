.. ISAcreator Documentation documentation master file, created by
   sphinx-quickstart on Wed Jan 15 12:48:03 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Contributing to ISAcreator
==========================

.. toctree::
   :maxdepth: 2



Building ISAcreator
----------

#. Clone the code to your machine. You may clone from the primary repository at ISA-tools/ISAcreator, or from your own fork.
#. Compile the code
	
	::

		mvn assembly:assembly -Dmaven.test.skip=true -Pbuild`)

	the build profile automatically sets some system variables like version etc. from information held within the pom.

#. Run the code
	
	::

		java -jar ISAcreator-<VERSION>.jar -Xms256m -Xmx1024m


Contribute
----------

You should read this article about `Github Flow <http://scottchacon.com/2011/08/31/github-flow.html>`_.

Ensure you have maven 2.2.1 installed and enabled as well as git. If you have trouble with dependencies, and you are running behind a proxy, please ensure you set the proxy in both MAVEN_OPTS and settings.xml. See `here <https://answers.atlassian.com/questions/31384/plugin-sdk-proxy-setting-for-https-is-not-working-but-http-is>`_ for more information.

#. Fork it.
#. Clone your forked repository to your machine
#. Create a branch off of the development branch (`git checkout -b myisacreator`)
#. Make and test your changes
#. Run the tests (`mvn clean test`)
#. Commit your changes (`git commit -am "Added something useful"`)
#. Push to the branch (`git push origin myisacreator`)
#. Create a `Pull Request <http://help.github.com/pull-requests/>`_ from your branch.


Some house keeping
----------

Before we can accept any contributions to ISAcreator, you need to sign a `CLA <http://en.wikipedia.org/wiki/Contributor_License_Agreement>`_:

Please email us at <isatools@googlegroups.com> to receive the CLA. Then you should sign this and send it back asap so we can add you to our development pool.

The purpose of this agreement is to clearly define the terms under which intellectual property has been contributed to ISAcreator and thereby allow us to defend the project should there be a legal dispute regarding the software at some future time.

For a list of contributors, please see `here <http://github.com/ISA-tools/ISAcreator/contributors>`_.

License
^^^^^^^^^^

CPAL License, available `here <http://isatab.sourceforge.net/licenses/ISAcreator-license.html>`_.